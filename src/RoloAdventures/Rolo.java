package RoloAdventures;

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

public class Rolo implements KeyboardHandler {
    private Picture rolo;
    private Field field = new Field();

    public Rolo() {
        rolo = new Picture(10,10,"/Users/codecadet/IdeaProjects/RoloAdventures/resources/rolo.png");
        rolo.draw();


        Keyboard kb = new Keyboard(this);

        KeyboardEvent right = new KeyboardEvent();
        right.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        right.setKey(KeyboardEvent.KEY_RIGHT);
        kb.addEventListener(right);

        KeyboardEvent left = new KeyboardEvent();
        left.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        left.setKey(KeyboardEvent.KEY_LEFT);
        kb.addEventListener(left);

        KeyboardEvent up = new KeyboardEvent();
        up.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        up.setKey(KeyboardEvent.KEY_UP);
        kb.addEventListener(up);

        KeyboardEvent down = new KeyboardEvent();
        down.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        down.setKey(KeyboardEvent.KEY_DOWN);
        kb.addEventListener(down);
    }


    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()){
            case KeyboardEvent.KEY_RIGHT:
                if (rolo.getX() <= field.getRightLimit() - 40) {
                    rolo.translate(40, 0);
                }
                break;
            case KeyboardEvent.KEY_LEFT:
                if (rolo.getX() >= 40) {
                    rolo.translate(-40, 0);
                }
                break;
            case KeyboardEvent.KEY_UP:
                if (getRoloY() >= 40) {
                    rolo.translate(0, -40);
                }
                break;
            case KeyboardEvent.KEY_DOWN:
                if (getRoloY() <= field.getBottoLimit() - 40) {
                    rolo.translate(0, 40);
                }
                break;
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

    }

    public int getRoloX(){
        return rolo.getX();
    }

    public int getRoloY(){
        return rolo.getY();
    }
}
